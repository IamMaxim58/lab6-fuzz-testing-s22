import { calculateBonuses } from "./bonus-system.js";

const assert = require("assert");


describe('Bonus tests', () => {
    console.log("Bonus system tests started");


    test('Standard test < 10000', (done) => {
        let bonus = Math.random() * 10000;
        assert.equal(calculateBonuses('Standard', bonus), 0.05);
        done();
    });
    test('Standard test 10000', (done) => {
        let bonus = 10000;
        assert.equal(calculateBonuses('Standard', bonus), 1.5 * 0.05);
        done();
    });
    test('Standard test < 50000', (done) => {
        let bonus = 10000 + Math.random() * 40000;
        assert.equal(calculateBonuses('Standard', bonus), 1.5 * 0.05);
        done();
    });
    test('Standard test 50000', (done) => {
        let bonus = 50000;
        assert.equal(calculateBonuses('Standard', bonus), 2 * 0.05);
        done();
    });
    test('Standard test < 100000', (done) => {
        let bonus = 50000 + Math.random() * 50000;
        assert.equal(calculateBonuses('Standard', bonus), 2 * 0.05);
        done();
    });
    test('Standard test 100000', (done) => {
        let bonus = 100000;
        assert.equal(calculateBonuses('Standard', bonus), 2.5 * 0.05);
        done();
    });
    test('Standard test > 100000', (done) => {
        let bonus = 100001 + Math.random() * 100000;
        assert.equal(calculateBonuses('Standard', bonus), 2.5 * 0.05);
        done();
    });


    test('Premium test < 10000', (done) => {
        let bonus = Math.random() * 10000;
        assert.equal(calculateBonuses('Premium', bonus), 0.1);
        done();
    });
    test('Premium test 10000', (done) => {
        let bonus = 10000;
        assert.equal(calculateBonuses('Premium', bonus), 1.5 * 0.1);
        done();
    });
    test('Premium test < 50000', (done) => {
        let bonus = 10000 + Math.random() * 40000;
        assert.equal(calculateBonuses('Premium', bonus), 1.5 * 0.1);
        done();
    });
    test('Premium test 50000', (done) => {
        let bonus = 50000;
        assert.equal(calculateBonuses('Premium', bonus), 2 * 0.1);
        done();
    });
    test('Premium test < 100000', (done) => {
        let bonus = 50000 + Math.random() * 50000;
        assert.equal(calculateBonuses('Premium', bonus), 2 * 0.1);
        done();
    });
    test('Premium test 100000', (done) => {
        let bonus = 100000;
        assert.equal(calculateBonuses('Premium', bonus), 2.5 * 0.1);
        done();
    });
    test('Premium test > 100000', (done) => {
        let bonus = 100001 + Math.random() * 100000;
        assert.equal(calculateBonuses('Premium', bonus), 2.5 * 0.1);
        done();
    });


    test('Diamond test < 10000', (done) => {
        let bonus = Math.random() * 10000;
        assert.equal(calculateBonuses('Diamond', bonus), 0.2);
        done();
    });
    test('Diamond test 10000', (done) => {
        let bonus = 10000;
        assert.equal(calculateBonuses('Diamond', bonus), 1.5 * 0.2);
        done();
    });
    test('Diamond test < 50000', (done) => {
        let bonus = 10000 + Math.random() * 40000;
        assert.equal(calculateBonuses('Diamond', bonus), 1.5 * 0.2);
        done();
    });
    test('Diamond test 50000', (done) => {
        let bonus = 50000;
        assert.equal(calculateBonuses('Diamond', bonus), 2 * 0.2);
        done();
    });
    test('Diamond test < 100000', (done) => {
        let bonus = 50000 + Math.random() * 50000;
        assert.equal(calculateBonuses('Diamond', bonus), 2 * 0.2);
        done();
    });
    test('Diamond test 100000', (done) => {
        let bonus = 100000;
        assert.equal(calculateBonuses('Diamond', bonus), 2.5 * 0.2);
        done();
    });
    test('Diamond test > 100000', (done) => {
        let bonus = 100001 + Math.random() * 100000;
        assert.equal(calculateBonuses('Diamond', bonus), 2.5 * 0.2);
        done();
    });


    test('Invalid test < 10000', (done) => {
        let bonus = Math.random() * 10000;
        assert.equal(calculateBonuses('Invalid', bonus), 0);
        done();
    });
    test('Invalid test 10000', (done) => {
        let bonus = 10000;
        assert.equal(calculateBonuses('Invalid', bonus), 0);
        done();
    });
    test('Invalid test < 50000', (done) => {
        let bonus = Math.random() * 50000;
        assert.equal(calculateBonuses('Invalid', bonus), 0);
        done();
    });
    test('Invalid test 50000', (done) => {
        let bonus = 50000;
        assert.equal(calculateBonuses('Invalid', bonus), 0);
        done();
    });
    test('Invalid test < 100000', (done) => {
        let bonus = Math.random() * 100000;
        assert.equal(calculateBonuses('Invalid', bonus), 0);
        done();
    });
    test('Invalid test 100000', (done) => {
        let bonus = 100000;
        assert.equal(calculateBonuses('Invalid', bonus), 0);
        done();
    });
    test('Invalid test > 100000', (done) => {
        let bonus = Math.random() * 100000;
        assert.equal(calculateBonuses('Invalid', bonus), 0);
        done();
    });
});
